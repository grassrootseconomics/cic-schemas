# @cicnet/schemas-data-validator

```javascript
import { validatePerson } from 'cic-schemas-data-validator'

const data = { data: 'that will not validate' }

const personValidationErrors = await validatePerson(data)

if (personValidationErrors) {
  personValidationErrors.map(e => console.log(`${e.message}`))
}
```
