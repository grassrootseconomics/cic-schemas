const test = require('tape')

const validatePerson = require('../src/index.js').validatePerson
const validateVcard = require('../src/index.js').validateVcard

test('Invalid person object - data is not an object', async function (t) {
  const data = []

  const person = await validatePerson(data)

  const expected = [
    {
      instancePath: '',
      schemaPath: '#/type',
      keyword: 'type',
      params: { type: 'object' },
      message: 'must be object'
    }
  ]

  t.deepEqual(person, expected, 'should fail validation')

  t.end()
})

test('Invalid person object - all properties missing', async function (t) {
  const data = {}

  const person = await validatePerson(data)

  const expected = [
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'date_registered' },
      message: "must have required property 'date_registered'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'gender' },
      message: "must have required property 'gender'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'identities' },
      message: "must have required property 'identities'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'location' },
      message: "must have required property 'location'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'products' },
      message: "must have required property 'products'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'vcard' },
      message: "must have required property 'vcard'"
    }
  ]

  t.deepEqual(person, expected, 'should fail validation')

  t.end()
})

test('Invalid person object - missing registration date', async function (t) {
  const data = {
    gender: 'female',
    identities: {
      evm: {
        'bloxberg:8996': ['ethAddress'],
        'oldchain:1': ['someOtherAddress']
      }
    },
    location: {},
    products: ['water'],
    vcard: 'someEncodedData'
  }

  const person = await validatePerson(data)

  const expected = [
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'date_registered' },
      message: "must have required property 'date_registered'"
    }
  ]

  t.deepEqual(person, expected, 'should fail validation')

  t.end()
})

test('Invalid person object - missing year in date of birth', async function (t) {
  const data = {
    date_registered: 1619212745519,
    date_of_birth: {
      day: 13,
      month: 6
    },
    gender: 'female',
    identities: {
      evm: {
        'bloxberg:8996': ['ethAddress'],
        'oldchain:1': ['someOtherAddress']
      }
    },
    location: {},
    products: ['water'],
    vcard: 'someEncodedData'
  }

  const person = await validatePerson(data)

  const expected = [
    {
      instancePath: '/date_of_birth',
      schemaPath: '#/properties/date_of_birth/required',
      keyword: 'required',
      params: { missingProperty: 'year' },
      message: "must have required property 'year'"
    }
  ]

  t.deepEqual(person, expected, 'should fail validation')

  t.end()
})

test('Invalid person object - missing Bloxberg address', async function (t) {
  const data = {
    date_registered: 1619212745519,
    gender: 'female',
    identities: {
      evm: {
        'oldchain:1': ['someOtherAddress']
      }
    },
    location: {},
    products: ['water'],
    vcard: 'someEncodedData'
  }

  const person = await validatePerson(data)

  const expected = [
    {
      instancePath: '/identities/evm',
      schemaPath: '#/properties/identities/properties/evm/required',
      keyword: 'required',
      params: { missingProperty: 'bloxberg:8996' },
      message: "must have required property 'bloxberg:8996'"
    }
  ]

  t.deepEqual(person, expected, 'should fail validation')

  t.end()
})

test('Valid person object', async function (t) {
  const data = {
    date_registered: 1619212745519,
    date_of_birth: {
      year: 1974,
      month: 6
    },
    gender: 'female',
    identities: {
      evm: {
        'bloxberg:8996': ['ethAddress'],
        'oldchain:1': ['someOtherAddress']
      }
    },
    location: {},
    products: ['water'],
    vcard: 'someEncodedData'
  }

  const person = await validatePerson(data)

  const expected = undefined

  t.deepEqual(person, expected, 'should pass validation')

  t.end()
})

test('Invalid vcard object - all properties missing', async function (t) {
  const data = {}

  const vcard = await validateVcard(data)

  const expected = [
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'fn' },
      message: "must have required property 'fn'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'n' },
      message: "must have required property 'n'"
    },
    {
      instancePath: '',
      schemaPath: '#/required',
      keyword: 'required',
      params: { missingProperty: 'tel' },
      message: "must have required property 'tel'"
    }
  ]

  t.deepEqual(vcard, expected, 'should fail validation')

  t.end()
})

test('Invalid vcard object - invalid email address - no dot', async function (t) {
  const data = {
    email: [
      {
        value: 'dude@devnull'
      }
    ],
    fn: [
      {
        value: 'John Doe'
      }
    ],
    n: [
      {
        value: ['John', 'Doe', '', 'Mr.']
      }
    ],
    tel: [
      {
        meta: {
          TYP: ['work, voice']
        },
        value: '+1234567890'
      }
    ],
    version: [
      {
        value: '3.0'
      }
    ]
  }

  const vcard = await validateVcard(data)

  const expected = [
    {
      instancePath: '/email/0/value',
      schemaPath: '#/properties/email/items/properties/value/pattern',
      keyword: 'pattern',
      params: { pattern: '^\\S+@\\S+\\.\\S+$' },
      message: 'must match pattern "^\\S+@\\S+\\.\\S+$"'
    }
  ]

  t.deepEqual(vcard, expected, 'should fail validation')

  t.end()
})

test('Invalid vcard object - invalid email address - spaces', async function (t) {
  const data = {
    email: [
      {
        value: 'dude @dev.null'
      }
    ],
    fn: [
      {
        value: 'John Doe'
      }
    ],
    n: [
      {
        value: ['John', 'Doe', '', 'Mr.']
      }
    ],
    tel: [
      {
        meta: {
          TYP: ['work, voice']
        },
        value: '+1234567890'
      }
    ],
    version: [
      {
        value: '3.0'
      }
    ]
  }

  const vcard = await validateVcard(data)

  const expected = [
    {
      instancePath: '/email/0/value',
      schemaPath: '#/properties/email/items/properties/value/pattern',
      keyword: 'pattern',
      params: { pattern: '^\\S+@\\S+\\.\\S+$' },
      message: 'must match pattern "^\\S+@\\S+\\.\\S+$"'
    }
  ]

  t.deepEqual(vcard, expected, 'should fail validation')

  t.end()
})

test('Invalid vcard object - invalid email address - no @', async function (t) {
  const data = {
    email: [
      {
        value: 'dude#dev.null'
      }
    ],
    fn: [
      {
        value: 'John Doe'
      }
    ],
    n: [
      {
        value: ['John', 'Doe', '', 'Mr.']
      }
    ],
    tel: [
      {
        meta: {
          TYP: ['work, voice']
        },
        value: '+1234567890'
      }
    ],
    version: [
      {
        value: '3.0'
      }
    ]
  }

  const vcard = await validateVcard(data)

  const expected = [
    {
      instancePath: '/email/0/value',
      schemaPath: '#/properties/email/items/properties/value/pattern',
      keyword: 'pattern',
      params: { pattern: '^\\S+@\\S+\\.\\S+$' },
      message: 'must match pattern "^\\S+@\\S+\\.\\S+$"'
    }
  ]

  t.deepEqual(vcard, expected, 'should fail validation')

  t.end()
})

test('Valid vcard object', async function (t) {
  const data = {
    email: [
      {
        value: 'dude@dev.null'
      }
    ],
    fn: [
      {
        value: 'John Doe'
      }
    ],
    n: [
      {
        value: ['John', 'Doe', '', 'Mr.']
      }
    ],
    tel: [
      {
        meta: {
          TYP: ['work, voice']
        },
        value: '+1234567890'
      }
    ],
    version: [
      {
        value: '3.0'
      }
    ]
  }

  const vcard = await validateVcard(data)

  const expected = undefined

  t.deepEqual(vcard, expected, 'should pass validation')

  t.end()
})
