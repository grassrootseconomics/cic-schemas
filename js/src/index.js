const Ajv = require('ajv')
const RefParser = require('@apidevtools/json-schema-ref-parser')

async function parser(data) {
  try {
    return await RefParser.dereference(data)
  } catch (error) {
    console.error('RefParser', error)
  }
}

async function validateSchema(data, schema) {
  const ajv = new Ajv({ allErrors: true })
  const parsedSchema = await parser(schema)
  const validate = ajv.compile(parsedSchema)
  const valid = validate(data)
  if (valid) {
    return
  } else {
    return validate.errors
  }
}

async function validatePerson(data) {
  return validateSchema(data, 'https://ic3.dev/cic/person-v1.json')
}

async function validateVcard(data) {
  return validateSchema(data, 'https://ic3.dev/cic/vcard_json-v1.json')
}

module.exports = { validatePerson, validateVcard }
