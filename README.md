# cic-schemas

JSON Schemas for data in the CIC network.

## Schema Location

JSON Schema validators and reference parsers work best when the schemas are available through a public URL. 

Schemas (and eventually fields too) will be uploaded to: https://schema.grassrootseconomics.net:8433

Example: https://schema.grassrootseconomics.net:8433/person-v1.json

## Versioning

Each schema has a version tag in its name (e.g., `person-v1.json`), so that the URL references for each version can be unique in order to enable backward compatibility.

Schemas and fields have an incremental version number when breaking changes are introduced to the schema. What constitutes a breaking change can vary and needs to be thought about carefully whenever changes are made to schemas and/or the fields of which they are composed.

### Non-Breaking Change Examples

- Adding a new data field to a schema that **IS NOT** required
- Changing an unrequired field to required
- Changing a field's validation requirements (e.g., setting the `maxLength` of a field to be LONGER than previously or the `minLength` to be SHORTER)
- 
### Breaking Change Examples

- Adding a new data field to a schema that **IS** required
- Changing a required field to unrequired
- Changing the type of a data field (e.g., converting string to an array of strings)
- Changing a field's validation requirements (e.g., setting the `maxLength` of a field to be SHORTER than previously or the `minLength` to be LONGER)
- Changing the validation pattern on a field 

The above two lists are non-exhaustive; there will be other examples for each of them, so all schema changes need to be thought about carefully to determine whether or not they are breaking changes.
